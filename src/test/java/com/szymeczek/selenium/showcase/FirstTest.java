package com.szymeczek.selenium.showcase;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

class FirstTest {

    private WebDriver driver;

    @BeforeAll
    static void beforeAll() {
        WebDriverManager.chromedriver().browserVersion("115.0.5790.170").setup();
    }

    @BeforeEach
    void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);
    }

    @Test
    void useSendKeysTest() {
        driver.get("https://ultimateqa.com/simple-html-elements-for-automation/");

        driver.findElement(By.id("et_pb_contact_name_0")).sendKeys("Ziutek");
        driver.findElement(By.id("et_pb_contact_email_0")).sendKeys("Zaślepka");
        driver.findElement(By.name("et_builder_submit_button")).click();


    }

    @Test
    void radioButtonTest() {
        driver.get("https://ultimateqa.com/simple-html-elements-for-automation/");

        driver.findElement(By.cssSelector("[name='gender'][value=male]")).click();
        driver.findElement(By.cssSelector("[name='vehicle'][value=Bike]")).click();

        Select carSelect = new Select(driver.findElement(By.tagName("select")));

        carSelect.selectByIndex(1);

    }

    @AfterEach
    void tearDown() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }
}
