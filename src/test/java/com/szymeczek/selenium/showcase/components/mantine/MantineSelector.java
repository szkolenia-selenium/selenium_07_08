package com.szymeczek.selenium.showcase.components.mantine;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;

import java.util.List;

public class MantineSelector implements ISelect {
    private final WebDriver driver;
    private final WebElement rootElement;

    public MantineSelector(WebDriver driver, String selectorCss) {
        this.driver = driver;
        rootElement = driver.findElement(By.cssSelector(String.format(".mantine-Select-root:has(%s)", selectorCss)));
    }

    @Override
    public boolean isMultiple() {
        return false;
    }

    @Override
    public List<WebElement> getOptions() {
        return null;
    }

    @Override
    public List<WebElement> getAllSelectedOptions() {
        return null;
    }

    @Override
    public WebElement getFirstSelectedOption() {
        return null;
    }

    @Override
    public void selectByVisibleText(String text) {

        rootElement.click();
        driver.findElements(By.className("mantine-Select-item"))
                .stream()
                .filter(a->text.contains(a.getText()))
                .findFirst()
                .get()
                .click();

    }

    @Override
    public void selectByIndex(int index) {

    }

    @Override
    public void selectByValue(String value) {

    }

    @Override
    public void deselectAll() {

    }

    @Override
    public void deselectByValue(String value) {

    }

    @Override
    public void deselectByIndex(int index) {

    }

    @Override
    public void deselectByVisibleText(String text) {

    }
}
