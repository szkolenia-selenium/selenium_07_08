package com.szymeczek.selenium.showcase.comarchshop;

import com.szymeczek.selenium.showcase.galaxy.AbstractUITTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;

class ImportFileTest extends AbstractUITTest {

    public static final By POPUP_MESSAGE_HEADER_LOCATOR = By.cssSelector(".after-adding-to-cart-popup-ui .line-height-1-ui");

    @Test
    void importBucketFromCSV() {
        driver.get("https://demob2b.comarch-esklep.pl/logowanie/7");

        login("cdntest@wp.pl", "Abc12345!");

        driver.findElement(By.id("file"))
                .sendKeys(new File("src/test/resources/upload/import/cart_importcsv_file.csv")
                        .getAbsolutePath());

        new WebDriverWait(driver, Duration.ofSeconds(1)).until(ExpectedConditions.visibilityOfElementLocated(POPUP_MESSAGE_HEADER_LOCATOR));
        Assertions.assertEquals(
                "Dodano do koszyka",
                driver.findElement(POPUP_MESSAGE_HEADER_LOCATOR).getText());
    }

    private void login(String emial, String password) {
        driver.findElement(By.name("email")).sendKeys(emial);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.className("login-button-ui")).click();
    }
}
