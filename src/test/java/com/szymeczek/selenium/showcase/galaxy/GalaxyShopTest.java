package com.szymeczek.selenium.showcase.galaxy;

import com.szymeczek.selenium.showcase.components.mantine.MantineSelector;
import com.szymeczek.selenium.showcase.galaxy.components.AddressComponent;
import com.szymeczek.selenium.showcase.galaxy.pageobject.DeliveryAddressPage;
import com.szymeczek.selenium.showcase.galaxy.pageobject.PayMethodPage;
import com.szymeczek.selenium.showcase.galaxy.pageobject.ProductListPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

class GalaxyShopTest extends AbstractUITTest {

    public static final By INPUT_FILED_TAX_ID_LOCATOR = By.name("taxId");
    public static final By INPUT_NAME_ADDRESS_POST_CODE_LOCATOR = By.name("address.postCode");
    public static final By INPUT_FILED_ADDRESS_PLACE_LOCATOR = By.name("address.place");
    public static final By SUB_HEADER_LOCATOR = By.cssSelector("[data-test=sub-header]");

    @Test
    void shoutOpenProductList() {

        driver.get(SeleniumHooks.basicHost("/index.html"));
        driver.findElement(By.linkText("Zaczynamy")).click();
        Assertions.assertEquals(SeleniumHooks.basicHost("/index.html#/products"), driver.getCurrentUrl());

    }

    @Test
    void buyAndPayFirstProduct() {
        DeliveryAddressPage deliveryPage = new ProductListPage(driver)
                .get().buyFirstProduct();

        deliveryPage.getTaxId().sendKeys("555555111111");
        deliveryPage.getAddress()
                .streetNumber("1")
                .postCode("30-333")
                .place("Kraków")
                .country("Polska");

        PayMethodPage payMethodPage = deliveryPage.send();
        Assertions.assertEquals("Metody płatności", payMethodPage.getTextHeader());
    }

    @Test
    void buyAndPayFirstProductValidationEmptyStreetNumber() {
        driver.get(SeleniumHooks.basicHost("/index.html#/products"));
        driver.findElement(By.cssSelector("[data-test=buy-and-pay-49963fe1-5339-4f87-926e-82c890da27c4]")).click();
        driver.findElement(INPUT_FILED_TAX_ID_LOCATOR).sendKeys("55555555");

        new AddressComponent("address", driver)
                .postCode("30-333")
                .country("Polska")
        ;
        driver.findElement(By.name("delivery-form")).submit();

        Assertions.assertEquals("Numer jest wymagany", findErrorMessageOnFieldTypeInput(driver, "address.streetNumber" ) );
        Assertions.assertEquals("Miejscowość jest wymagana", findErrorMessageOnFieldTypeInput(driver, "address.place" ) );


    }
    private String findErrorMessageOnFieldTypeInput(WebDriver driver, String inputName) {
        return driver.findElement(By.cssSelector(String.format(".mantine-TextInput-root:has([name='%s'])", inputName)))
                .findElement(By.className("mantine-TextInput-error")).getText();
    }


    @Test
    void buyAndPayFirstIndividualProduct() {
        driver.get(SeleniumHooks.basicHost("/index.html#/products"));
        driver.findElement(By.cssSelector("[data-test=buy-and-pay-49963fe1-5339-4f87-926e-82c890da27c4]")).click();
        driver.findElement(By.name("individual")).click();

        new AddressComponent("addressDelivery", driver)
                .streetNumber("1")
                .postCode("21-107")
                .place("Serniki")
                .country("Polska");
    }
}
