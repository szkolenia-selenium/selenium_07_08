package com.szymeczek.selenium.showcase.galaxy.pageobject;

import com.szymeczek.selenium.showcase.galaxy.SeleniumHooks;
import com.szymeczek.selenium.showcase.galaxy.components.AddressComponent;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

@Getter
public class DeliveryAddressPage extends LoadableComponent<DeliveryAddressPage> {
    private final WebDriver driver;
    private final AddressComponent address;

    @FindBy(name = "taxId")
    private WebElement taxId;

    @FindBy(name = "delivery-form")
    private WebElement form;

    public DeliveryAddressPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        address =  new AddressComponent("address", driver);
    }
    public PayMethodPage send(){
        form.submit();
       return new PayMethodPage(driver);
    }

    @Override
    protected void load() {
        driver.get(SeleniumHooks.basicHost("/index.html#/products/49963fe1-5339-4f87-926e-82c890da27c4/buy-and-pay"));
    }

    @Override
    protected void isLoaded() throws Error {
        Assertions.assertEquals("Kupuję i płacę", driver.findElement(By.cssSelector("[data-test=header]")).getText());
    }
}
