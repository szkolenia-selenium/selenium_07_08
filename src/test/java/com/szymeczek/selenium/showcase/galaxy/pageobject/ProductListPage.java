package com.szymeczek.selenium.showcase.galaxy.pageobject;

import com.szymeczek.selenium.showcase.galaxy.SeleniumHooks;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import java.util.List;
@Getter
public class ProductListPage extends LoadableComponent<ProductListPage> {


    @FindBy(css = "[data-test^=buy-and-pay]")
    private List<WebElement> buttonsQuickBuy;

    private final WebDriver driver;

    public DeliveryAddressPage buyFirstProduct(){
        driver.findElement(By.cssSelector("[data-test^=buy-and-pay]")).click();
        return new DeliveryAddressPage(driver);
    }

    public ProductListPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    protected void load() {
        driver.get(SeleniumHooks.basicHost("/index.html#/products"));
    }

    @Override
    protected void isLoaded() throws Error {
        Assertions.assertEquals(SeleniumHooks.basicHost("/index.html#/products"), driver.getCurrentUrl());
    }

}
