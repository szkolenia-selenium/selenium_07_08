package com.szymeczek.selenium.showcase.galaxy.components;

import com.szymeczek.selenium.showcase.components.mantine.MantineSelector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class AddressComponent {
   private final String prefixComponent;
    private final WebDriver driver;

    public AddressComponent(String prefixComponent, WebDriver driver) {
        this.prefixComponent = prefixComponent;
        this.driver = driver;
    }

    public AddressComponent streetNumber(String text){
        driver.findElement(By.name(prefixComponent+".streetNumber")).sendKeys(text);
        return this;
    }
    public AddressComponent postCode(String text){
        driver.findElement(By.name(prefixComponent+".postCode")).sendKeys(text);
        return this;
    }
    public AddressComponent place(String text){
        driver.findElement(By.name(prefixComponent+ ".place")).sendKeys(text);
        return this;
    }
    public AddressComponent country(String text){

        new MantineSelector(driver, "[name='"+prefixComponent+".country']").selectByVisibleText(text);
        return this;
    }


}
