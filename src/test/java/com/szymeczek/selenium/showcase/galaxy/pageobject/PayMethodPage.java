package com.szymeczek.selenium.showcase.galaxy.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PayMethodPage {
    public static final By SUB_HEADER_LOCATOR = By.cssSelector("[data-test=sub-header]");
    private final WebDriver driver;

    @FindBy(css = "[data-test=sub-header]")
    private WebElement header;

    public PayMethodPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTextHeader() {
        new WebDriverWait(driver, Duration.ofSeconds(1))
                .until(
                        ExpectedConditions.textToBePresentInElementLocated(
                                SUB_HEADER_LOCATOR, "Metody płatności")
                );
        return header.getText();
    }
}
