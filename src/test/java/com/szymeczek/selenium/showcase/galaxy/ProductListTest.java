package com.szymeczek.selenium.showcase.galaxy;

import com.github.javafaker.Faker;
import com.szymeczek.selenium.showcase.galaxy.pageobject.DeliveryAddressPage;
import com.szymeczek.selenium.showcase.galaxy.pageobject.ProductListPage;
import org.junit.jupiter.api.Test;

public class ProductListTest extends AbstractUITTest{
    @Test
    void startProcesBuy() {
        DeliveryAddressPage deliveryPage = new ProductListPage(driver)
                .get().buyFirstProduct();

        deliveryPage.getTaxId().clear();
        deliveryPage.getTaxId().sendKeys(new Faker().number().digits(10));
    }
}
